package com.itau.fizzbuzz;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
//		if (i % 3 == 0) {
//			return "fizz";
//		}
//		return null;
		if ((i % 3 ==0) && (i % 5 ==0))  {
			return "FizzBuzz";
		}
		
		if (i % 5 == 0) {
			return "Buzz";
		}
		
		if ( i % 3 == 0) {
			return "Fizz";
			
		}

		return Integer.toString(i);
		
		
	}
	
	public static String novaFuncao(int n) {
	String resp = "";
	for (int i = 1; i <= n; i++) {
		if ((i % 3 ==0) && (i % 5 ==0))  {
			resp = (resp + " FizzBuzz");
		} else if (i % 5 == 0) {
			resp = (resp + " Buzz");
		} else if ( i % 3 == 0) {
			resp = (resp + " Fizz");
			
		}else {
			resp = (resp + " " + Integer.toString(i));
		}
	}

		return resp.trim();
	}

	
}
